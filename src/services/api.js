import axios from "axios";

// https://viacep.com.br/ws/seucep/json/

const api = axios.create({
    baseURL: "https://viacep.com.br/ws/"
})

export default api;